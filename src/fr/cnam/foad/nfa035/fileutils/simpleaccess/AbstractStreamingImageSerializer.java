package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {
    @Override
    public final void serialize(S source, M media) throws IOException {
        getSourceInputStream(source).transferTo(getSerializingStream(media));
    }

    private OutputStream getSerializingStream(M media) {
        // TODO: code
        return null;
    }

    private InputStream getSourceInputStream(S source) {
        // TODO: code
        return null;
    }
}
