package NFA035.exercice02.src.fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.IOException;

public interface ImageStreamingSerializer<S, M> {
    void serialize(S source, M media) throws IOException;
}

